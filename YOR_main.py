import pyttsx3
import speech_recognition as sr
import datetime
import requests
from bs4 import BeautifulSoup
import webbrowser
import pyautogui
import os
from flask import Flask, jsonify
from database import add_hotkey, recognize_speech, execute_command, get_all_hotkeys
import keyword

app = Flask(__name__)

engine = pyttsx3.init("sapi5")
voices = engine.getProperty("voices")
engine.setProperty("voice", voices[0].id)
rate = engine.setProperty("rate", 170)


def speak(audio):
    engine.say(audio)
    engine.runAndWait()


def take_command():
    r = sr.Recognizer()
    with sr.Microphone() as source:
        print("Listening...")
        r.pause_threshold = 1
        r.energy_threshold = 300
        audio = r.listen(source, timeout=50)

    try:
        print("Recognizing...")
        query = r.recognize_google(audio, language='en-us')
        print(f"You said: {query}\n")
    except Exception as e:
        print(e)
        print("Sorry, I couldn't understand the command.")
        query = input("Your command: ").lower()
    return query


# Greet function
@app.route('/greet', methods=['GET'])
def greet():
    speak("Hello! I am your virtual assistant.")
    return "Greeted!"


# Function to get all hotkeys
@app.route('/hotkeys', methods=['GET'])
def get_hotkeys():
    hotkeys = get_all_hotkeys()
    return jsonify(hotkeys)


# Main function
if __name__ == "__main__":
    print("Virtual Assistant is ready. You can start interacting.")
    while True:
        # Alternately check for speech input and text input
        query = take_command().lower()

        if query == "wake up":
            add_hotkey("copy", "ctrl+c")
            add_hotkey("paste", "ctrl+v")
            speak("I'm awake! How can I assist you?")

        elif query == "exit":
            speak("Okay, goodbye!")
            break

        elif query == "hotkey":
            speak("Please say the command you want to execute.")
            user_input = take_command()
            if user_input:
                execute_command(user_input)

        elif query == "hello":
            speak("Hello! How are you?")

        elif query == "i am fine":
            speak("That's great to hear!")

        elif query == "how are you":
            speak("I'm functioning perfectly, thank you for asking!")

        elif query == "thank you":
            speak("You're welcome!")

        elif query == "how's your day":
            speak("My day is going well, thank you for asking!")

        elif query == "tell me a joke":
            speak("Why don't scientists trust atoms? Because they make up everything!")

        elif query == "what is your favorite colour":
            speak("I don't have a favorite color, but I appreciate all colors equally!")

        elif query == "do you have any hobbies":
            speak("My main hobby is assisting you!")

        elif query == "can you sing":
            speak("I'm not programmed to sing, but I can definitely help you find some great music!")

        elif query == "do you like movies":
            speak("I don't have personal preferences, but I can help you find information about movies!")

        elif query == "what's the meaning of life":
            speak("That's a deep question! The meaning of life is subjective and can vary from person to person.")

        elif query == "do you believe in aliens":
            speak("As an artificial intelligence, I don't have beliefs, but the universe is vast, so it's certainly possible!")

        elif query == "tell me a fun fact":
            speak(
                "Did you know that honey never spoils? Archaeologists have found pots of honey in ancient Egyptian tombs that are over 3,000 years old and still perfectly edible!")

        elif "open" in query:
            if "visual studio" in query:
                os.system("start code")
            else:
                from Dictapp import openappweb
                openappweb(query)

        elif "close" in query:
            from Dictapp import closeappweb
            closeappweb(query)

        elif "pause" in query:
            pyautogui.press("k")
            speak("video paused")
        elif "play" in query:
            pyautogui.press("k")
            speak("video played")
        elif "mute" in query:
            pyautogui.press("m")
            speak("video muted")

        elif "volume up" in query:
            from keyboard import volumeup

            speak("Turning volume up,sir")
            volumeup()
        elif "volume down" in query:
            from keyboard import volumedown

            speak("Turning volume down, sir")
            volumedown()

        elif "google" in query:
            from SearchNow import searchGoogle
            searchGoogle(query)

        elif "youtube" in query:
            from SearchNow import searchYoutube
            searchYoutube(query)

        elif "wikipedia" in query:
            from SearchNow import searchWikipedia
            searchWikipedia(query)

        elif "temperature" in query:
            search = "temperature in delhi"
            url = f"https://www.google.com/search?q={search}"
            r = requests.get(url)
            data = BeautifulSoup(r.text, "html.parser")
            temp = data.find("div", class_="BNeawe").text
            speak(f"current{search} is {temp}")

        elif "weather" in query:
            search = "temperature in delhi"
            url = f"https://www.google.com/search?q={search}"
            r = requests.get(url)
            data = BeautifulSoup(r.text, "html.parser")
            temp = data.find("div", class_="BNeawe").text
            speak(f"current{search} is {temp}")

        elif "the time" in query:
            strTime = datetime.datetime.now().strftime("%H:%M")
            speak(f"Sir, the time is {strTime}")

        elif "finally sleep" in query:
            speak("Going to sleep, sir")
            exit()



        # Run Flask app
        app.run(debug=True)
