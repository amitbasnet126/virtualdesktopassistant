import pymongo
import speech_recognition as sr
import keyboard

# Connect to MongoDB
client = pymongo.MongoClient("mongodb://localhost:27017")
db = client["hotkeys"]
collection = db["hotkeys"]

# Function to add a hotkey to the database
def add_hotkey(hotkey_name, command):
    hotkey = {"hotkey_name": hotkey_name, "command": command}
    collection.insert_one(hotkey)

# Function to retrieve all hotkeys from the database
# Adding a new function to get all hotkeys
def get_all_hotkeys():
    return list(collection.find({}, {"_id": 0}))


# Function to recognize speech
def recognize_speech():
    recognizer = sr.Recognizer()
    with sr.Microphone() as source:
        print("Listening for command...")
        audio = recognizer.listen(source)
    try:
        command = recognizer.recognize_google(audio)
        return command.lower()
    except sr.UnknownValueError:
        print("Sorry, I couldn't understand the command.")
        return ""
    except sr.RequestError:
        print("Sorry, there was an issue with the speech recognition service.")
        return ""

# Function to execute the command associated with a hotkey
def execute_command(command):
    hotkeys = get_all_hotkeys()
    for hotkey in hotkeys:
        if command in hotkey["hotkey_name"].lower():
            keyboard.press_and_release(hotkey["command"])
            print("Executed hotkey:", hotkey["hotkey_name"])
            return
    print("No matching hotkey found for command:", command)
